from django.contrib import admin
from django.urls import path, include
from django.conf.urls import handler400, handler403, handler404, handler500

urlpatterns = [
    path('blog/', include('blogy.urls')),
    path('admin/', admin.site.urls),
]

handler400 = 'blogy.views.bad_request'
handler403 = 'blogy.views.permission_denied'
handler404 = 'blogy.views.page_not_found'
handler500 = 'blogy.views.server_error'
