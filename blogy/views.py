from datetime import datetime
from django.shortcuts import render, get_object_or_404, redirect
from django.http import Http404
from blogy.models import Post
from .forms import ContactForm, LoginForm, PostForm
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.utils.translation import ungettext
from django.utils import timezone
from django.contrib.auth.models import User


def Date(request):
    return render(request, 'date.html', {'date': datetime.now()})

def Addition(request, nombre1, nombre2):    
    total = nombre1 + nombre2
    return render(request, 'addition.html', locals())

def Home(request):
    all_post = Post.objects.all()
    return render(request, 'home.html', {'all_post': all_post})

def Read(request, id):
    post = get_object_or_404(Post, id=id)
    return render(request, 'read.html', {'post':post})

def Contact(request):
    form = ContactForm(request.POST or None)
    if form.is_valid(): 
        topic = form.cleaned_data['topic']
        text = form.cleaned_data['text']
        sender = form.cleaned_data['sender']
        sending = True
    
    return render(request, 'contact.html', locals())

def Login(request):
    error = False

    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            if user: 
                login(request, user)  
            else: 
                error = True
    else:
        form = LoginForm()

    return render(request, 'login.html', locals())

def Logout(request):
    logout(request)
    return redirect(reverse(Home))

#def test_i18n(request):
#    nb_chats = 2
#    couleur = "blanc"
#    chaine = _("J'ai un %(animal)s %(col)s.") % {'animal': 'chat', 'col': couleur}
#    infos = ungettext(
#        "… et selon mes informations, vous avez %(nb)s chat %(col)s !",
#        "… et selon mes informations, vous avez %(nb)s chats %(col)ss !",
#        nb_chats) % {'nb': nb_chats, 'col': couleur}
#
#    return render(request, 'test_i18n.html', locals())

def New(request):
    if request.method == "POST":
        form = PostForm(request.POST) #or None, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.date = timezone.now()
            post.save()
        return render(request, 'home.html', {'form': form})
    else:
        form = PostForm()
    return render(request, 'new.html', {'form': form})

def Edit(request, id):
    post = get_object_or_404(Post, id=id)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post) #or None, request.FILES,
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.date = timezone.now()
            post.save()
#            if User.is_authentified and User.username == article.auteur:
 #               article.auteur.is_connected = True
        return render(request, 'read.html', {'post': post})
    else:
        form = PostForm(instance=post)
    return render(request, 'edit.html', {'form': form, 'post': post})


def bad_request(request, exception):
    return render(request, 'bad_request.html', locals())

def permission_denied(request, exception):
    return render(request, 'permission_denied.html', locals())

def page_not_found(request, exception):
    return render(request, 'page_not_found.html', locals())

def server_error(request):
    return render(request, 'server_error.html', locals())
