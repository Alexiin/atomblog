from django.urls import path
from . import views

urlpatterns = [
    path('', views.Home, name='home'),
    path('post/<int:id>', views.Read, name='read'),
    path('contact/', views.Contact, name='contact'),
    path('login/', views.Login, name='login'),
    path('logout/', views.Logout, name='logout'),
    path('new_post/', views.New, name='new_post'),
    path('edit_post/<int:id>', views.Edit, name='edit_post'),
]
